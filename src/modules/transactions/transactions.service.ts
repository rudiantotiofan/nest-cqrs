import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TransactionDTO } from './interfaces/transaction-dto.interface';
import { Transaction } from './repository/transaction.entity';
import { TopUpCommand } from './command/impl/top-up.command';

@Injectable()
export class TransactionsService {

  constructor(
    private commandBus: CommandBus,
    private queryBus: QueryBus,

  ) { }


  async topUp(dto: TransactionDTO): Promise<Transaction> {
    return this.commandBus.execute(new TopUpCommand(dto));
  }

}
