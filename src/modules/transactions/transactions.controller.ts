import { Controller, Post, Body } from '@nestjs/common';
import { TransactionDTO } from './interfaces/transaction-dto.interface';
import { Transaction } from './repository/transaction.entity';
import { TransactionsService } from './transactions.service';

@Controller('transaction')
export class TransactionsController {

  constructor(
    private transactionService: TransactionsService,
  ){ }

  @Post('top-up')
  async store(@Body() dto: TransactionDTO): Promise<Transaction> {
    return await this.transactionService.topUp(dto);

  }

}
