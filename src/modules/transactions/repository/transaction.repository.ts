import { Repository, EntityRepository, getRepository } from 'typeorm';
import { Transaction } from './transaction.entity';
import { Transaction as TransactionModel } from '../models/transaction.model';
import { User } from 'src/modules/users/repository/user.entity';

@EntityRepository(Transaction)
export class TransactionRepository extends Repository<Transaction> {

  base() {
    return getRepository(Transaction)
      .createQueryBuilder('transaction');
  }

  async topUp(user: User, value: number): Promise<TransactionModel> {
    const data = new Transaction();
    data.user = user;
    data.value = value;
    return new TransactionModel(
      await this.manager.save(data)
    );
  }

}