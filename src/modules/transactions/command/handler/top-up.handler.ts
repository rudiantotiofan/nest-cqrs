import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TransactionRepository } from '../../repository/transaction.repository';
import { UserRepository } from 'src/modules/users/repository/user.repository';
import { TopUpCommand } from '../impl/top-up.command';

@CommandHandler(TopUpCommand)
export class TopUpHandler implements ICommandHandler<TopUpCommand> {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly transactionRepository: TransactionRepository,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: TopUpCommand) {
    console.log('transaction : top up command..');
    const { dto } = command;

    const user = await this.userRepository.findById(dto.userId);
    
    const transaction = this.publisher.mergeObjectContext(
      await this.transactionRepository.topUp(user, dto.value),
    );
    // call some events
    transaction.updateUserQuota();
    transaction.commit();
    return transaction;
  }
}
