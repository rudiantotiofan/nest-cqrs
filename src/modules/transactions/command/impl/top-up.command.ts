import { TransactionDTO } from '../../interfaces/transaction-dto.interface';

export class TopUpCommand {
  constructor(
    public readonly dto: TransactionDTO,
  ) {}
}
