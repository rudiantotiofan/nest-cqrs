import { AggregateRoot } from '@nestjs/cqrs';
import { Transaction as TransactionEntity } from '../repository/transaction.entity';
import { UpdateUserQuotaEvent } from '../events/impl/update-user-quota.event';

export class Transaction extends AggregateRoot {
  constructor(
    private readonly transaction: TransactionEntity,
  ) {
    super();
  }

  updateUserQuota() {
    this.apply(new UpdateUserQuotaEvent(this.transaction));
  }
}
