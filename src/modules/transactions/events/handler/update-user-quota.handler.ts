import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { UpdateUserQuotaEvent } from '../impl/update-user-quota.event';
import { UserRepository } from 'src/modules/users/repository/user.repository';

@EventsHandler(UpdateUserQuotaEvent)
export class UpdateUserQuotaHandler implements IEventHandler<UpdateUserQuotaEvent> {
  constructor(
    private userRepository: UserRepository,
  ){ }

  handle(event: UpdateUserQuotaEvent) {
    const { transaction } = event;
    console.log('transaction : call event update user quota ', transaction.user);
  }
}
