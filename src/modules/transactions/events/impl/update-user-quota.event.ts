import { Transaction } from "../../repository/transaction.entity";

export class UpdateUserQuotaEvent {
  constructor(
    public readonly transaction: Transaction,
  ) {}
}
