import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommandHandlers } from './command/handler';
import { EventHandlers } from './events/handler';
import { TransactionRepository } from './repository/transaction.repository';
import { UserRepository } from '../users/repository/user.repository';
import { TransactionsSagas } from './sagas/transactions.sagas';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([
      TransactionRepository,
      UserRepository,
    ]),
  ],
  providers: [
    TransactionsService,
    TransactionsSagas,
    ...CommandHandlers,
    ...EventHandlers,
  ],
  controllers: [TransactionsController]
})
export class TransactionsModule {}
