import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UpdateUserQuotaEvent } from '../events/impl/update-user-quota.event';
import { UpdateQuotaCommand } from 'src/modules/users/commands/impl/update-quota.command';

const itemId = '0';

@Injectable()
export class TransactionsSagas {
  @Saga()
  updateQuota = (events$: Observable<any>): Observable<ICommand> => {
    return events$
      .pipe(
        ofType(UpdateUserQuotaEvent),
        map(event => {
          console.log('sagas : call update quota command');
          return new UpdateQuotaCommand(event.transaction);
        }),
      );
  }
}
