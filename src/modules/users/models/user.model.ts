import { AggregateRoot } from '@nestjs/cqrs';
import { CreateNewUserEvent } from '../events/impl/create-new-user.event';
import { User as UserEntity} from '../repository/user.entity';
import { CreateUserDTO } from '../interfaces/create-dto.interface';

export class User extends AggregateRoot {
  constructor(
    private readonly user: UserEntity,
  ) {
    super();
  }

  createLog() {
    this.apply(new CreateNewUserEvent(this.user));
  }
}
