import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetUsersQuery } from '../impl/get-users.query';
import { UserRepository } from '../../repository/user.repository';

@QueryHandler(GetUsersQuery)
export class GetUserHandler implements IQueryHandler<GetUsersQuery> {
  constructor(private readonly userRepository: UserRepository) {}

  async execute(query: GetUsersQuery) {
    console.log('users : fetch all users');
    return this.userRepository.findAll();
  }
}
