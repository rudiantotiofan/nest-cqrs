import { Repository, EntityRepository, getRepository } from 'typeorm';
import { User } from "./user.entity";
import { User as UserModel } from "../models/user.model";
import { CreateUserDTO } from '../interfaces/create-dto.interface';
import { Transaction } from 'src/modules/transactions/repository/transaction.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {

  base() {
    return getRepository(User)
      .createQueryBuilder('user');
  }

  async findById(id: number): Promise<User> {
    return this.base()
      .andWhere('user.id = :userId', { userId: id })
      .getOne();
  }

  async findAll() {
    return this.base().getMany();
  }

  async store(dto: CreateUserDTO): Promise<UserModel> {
    const data = new User();
    data.name = dto.name;
    return new UserModel(
      await this.manager.save(data)
    );
  }

  async updateQuota(transaction: Transaction): Promise<UserModel> {
    const data = await getRepository(User).findOne(transaction.user.id);
    const total: number = Number(data.quota) + Number(transaction.value);
    data.quota = total;
    return new UserModel(
      await this.manager.save(data)
    );
  }

}