import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { UserRepository } from './repository/user.repository';
import { CommandHandlers } from './commands/handler';
import { EventHandlers } from './events/handler';
import { QueryHandlers } from './queries/handler';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([
      UserRepository,
    ]),
  ],
  providers: [
    UsersService,
    ...CommandHandlers,
    ...EventHandlers,
    ...QueryHandlers,
  ],
  controllers: [
    UsersController
  ]
})
export class UsersModule {}
