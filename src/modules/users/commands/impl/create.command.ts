import { ICommand } from '@nestjs/cqrs';
import { CreateUserDTO } from "../../interfaces/create-dto.interface";

export class CreateCommand implements ICommand {
  constructor(
    public readonly dto: CreateUserDTO,
  ) {}
}
