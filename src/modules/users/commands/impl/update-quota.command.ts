import { ICommand } from '@nestjs/cqrs';
import { Transaction } from 'src/modules/transactions/repository/transaction.entity';

export class UpdateQuotaCommand implements ICommand {
  constructor(
    public readonly transaction: Transaction,
  ) {}
}
