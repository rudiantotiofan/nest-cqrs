import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { UserRepository } from '../../repository/user.repository';
import { CreateCommand } from '../impl/create.command';

@CommandHandler(CreateCommand)
export class CreateHandler implements ICommandHandler<CreateCommand> {
  constructor(
    private readonly repository: UserRepository,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: CreateCommand) {
    console.log('users : create user command..');

    const { dto } = command;
    const user = this.publisher.mergeObjectContext(
      await this.repository.store(dto),
    );
    // call some events
    user.createLog();
    user.commit();
    return user;
  }
}
