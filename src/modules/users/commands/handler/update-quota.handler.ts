import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { UserRepository } from '../../repository/user.repository';
import { CreateCommand } from '../impl/create.command';
import { UpdateQuotaCommand } from '../impl/update-quota.command';

@CommandHandler(UpdateQuotaCommand)
export class UpdateQuotaHandler implements ICommandHandler<UpdateQuotaCommand> {
  constructor(
    private readonly repository: UserRepository,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: UpdateQuotaCommand) {
    console.log('users : update user quota command..');

    const { transaction } = command;
    const user = this.publisher.mergeObjectContext(
      await this.repository.updateQuota(transaction),
    );
    user.commit();
  }
}
