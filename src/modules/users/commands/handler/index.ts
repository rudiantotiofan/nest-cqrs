import { CreateHandler } from './create.handler';
import { UpdateQuotaHandler } from './update-quota.handler';

export const CommandHandlers = [CreateHandler, UpdateQuotaHandler];
