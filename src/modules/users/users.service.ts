import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { UserRepository } from './repository/user.repository';
import { User } from './repository/user.entity';
import { CreateUserDTO } from './interfaces/create-dto.interface';
import { CreateCommand } from './commands/impl/create.command';
import { GetUsersQuery } from './queries/impl';

@Injectable()
export class UsersService {
  constructor(
    private commandBus: CommandBus,
    private queryBus: QueryBus,

  ) { }

  async sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
  }

  async findAll(): Promise<User[]> {
    return this.queryBus.execute(new GetUsersQuery());
  }

  async create(dto: CreateUserDTO): Promise<User> {
    return this.commandBus.execute(new CreateCommand(dto));
  }
}
