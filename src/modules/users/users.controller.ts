import { Controller, Get, Post, Body } from '@nestjs/common';
import { User } from './repository/user.entity';
import { UsersService } from './users.service';
import { CreateUserDTO } from './interfaces/create-dto.interface';

@Controller('user')
export class UsersController {
  constructor(
    private userService: UsersService,
  ) { }

  @Get('all')
  async findAll(): Promise<User[]> {
    const user = await this.userService.findAll();
    console.log('users : return controller');
    return user;
  }

  @Post('store')
  async store(@Body() dto: CreateUserDTO): Promise<User> {
    return await this.userService.create(dto);

  }

}
