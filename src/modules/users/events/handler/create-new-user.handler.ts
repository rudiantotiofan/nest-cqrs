import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { CreateNewUserEvent } from '../impl/create-new-user.event';

@EventsHandler(CreateNewUserEvent)
export class CreateNewUserHandler
  implements IEventHandler<CreateNewUserEvent> {
  handle(event: CreateNewUserEvent) {
    const { user } = event;
    console.log('users : create new user event ', user.name);
  }
}
