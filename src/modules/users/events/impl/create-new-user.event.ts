import { CreateUserDTO } from "../../interfaces/create-dto.interface";
import { User } from "../../repository/user.entity";

export class CreateNewUserEvent {
  constructor(
    public readonly user: User,
  ) {}
}
